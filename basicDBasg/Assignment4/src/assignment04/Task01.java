/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nhan
 */
public class Task01 {
    
    static BufferedReader br = null;
    static String line = "";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Scanner keyboard = new Scanner(System.in);
            int selection = 0;
            
            while ((selection > 7) || (selection < 1)){
                System.out.println("Task 01: Select the table you want to execeute update on or do all the update at once");
                System.out.println("Input for tables are stored in /data directory");
                System.out.println("=========================================");
                System.out.println("1. Students");
                System.out.println("2. Instructors");
                System.out.println("3. Courses");
                System.out.println("4. Grades");
                System.out.println("5. Advising");
                System.out.println("6. Do all update in order");
                System.out.println("7. Quit");
                System.out.println("=========================================");
                System.out.print("Enter your selection: ");
                selection = keyboard.nextInt();
            }
            
            switch (selection){
                case 1:
                    updateStudents();
                    break;
                    
                case 2:
                    updateInstructors();
                    break;
                    
                case 3:
                    updateCourses();
                    break;
                    
                case 4:
                    updateGrades();
                    break;
                    
                case 5:
                    updateAdvising();
                    break;
                    
                case 6:
                    updateStudents();
                    updateInstructors();
                    updateCourses();
                    updateGrades();
                    updateAdvising();
                    break;
                    
                case 7:
                    System.exit(0);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Task01.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | SQLException ex) {
            Logger.getLogger(Task01.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    static void updateStudents() throws FileNotFoundException, IOException, SQLException{
        br = preparedBufferedReader("data/student.txt");
        while ((line = br.readLine()) != null){
            Student stdn = new Student(line);
            Student.updateStudent(stdn);
            System.out.println(String.format("Processed student number %d", stdn.StudentNumber));
        }
        System.out.println("Finished update students without error!");
        Student.showStudents();
    }
    
    static void updateInstructors() throws FileNotFoundException, IOException, SQLException{
        br = preparedBufferedReader("data/instructor.txt");
        while ((line = br.readLine()) != null){
            Instructor instr = new Instructor(line);
            Instructor.updateInstructor(instr);
            System.out.println(String.format("Processed instructor number %d", instr.EmployeeNumber));
        }
        System.out.println("Finished update instructors without error!");
        Instructor.showInstructors();
    }
    
    static void updateCourses() throws FileNotFoundException, IOException, SQLException{
        br = preparedBufferedReader("data/course.txt");
        while ((line = br.readLine()) != null){
            Course course = new Course(line);
            Course.updateCourse(course);
            System.out.println(String.format("Processed course number %s", course.CourseNumber));
        }
        System.out.println("Finished update courses without error!");        
        Course.showCourses();
    }
    
    static void updateGrades() throws FileNotFoundException, IOException, SQLException{
        br = preparedBufferedReader("data/grade.txt");
        while ((line = br.readLine()) != null){
            Grade grade = new Grade(line);
            Grade.updateGrade(grade);

            System.out.println(String.format("Processed grade of student number %d and instructor number %d in course number %s", grade.StudentNumber, grade.EmployeeNumber, grade.CourseNumber));
        }
        System.out.println("Finished update grades without error!");        
        Grade.showGrades();
    }
    
    static void updateAdvising() throws FileNotFoundException, IOException, SQLException{
        br = preparedBufferedReader("data/advising.txt");
        while ((line = br.readLine()) != null){
            Advising adv = new Advising(line);
            Advising.updateAdvising(adv);

            System.out.println(String.format("Processed advising of student number %d and instructor number %d", adv.StudentNumber, adv.EmployeeNumber));
        }
        System.out.println("Finished update advising without error!");        
        Advising.showAdvising();
    }
    
    static BufferedReader preparedBufferedReader(String filepath) throws FileNotFoundException{
        FileInputStream fstream = new FileInputStream(filepath);
        DataInputStream in = new DataInputStream(fstream);
        return new BufferedReader(new InputStreamReader(in));
    }
}