/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class Instructor {
    public int EmployeeNumber;
    public String Name;
    public String Rank;
    public int RoomNo;
    public int TelNo;
    
    static Connection connection;
    static Statement statement;
    static ResultSet rs;

    public Instructor(int EmployeeNumber, String Name, String Rank, int RoomNo, int TelNo) {
        this.EmployeeNumber = EmployeeNumber;
        this.Name = Name;
        this.Rank = Rank;
        this.RoomNo = RoomNo;
        this.TelNo = TelNo;
    }
    
    public Instructor(String instructorString){
        String[] strings = instructorString.split(",");
        int empno = Integer.parseInt(strings[0]);
        int roomno = Integer.parseInt(strings[3]);
        int telno = Integer.parseInt(strings[4]);
        
        this.EmployeeNumber = empno;
        this.Name = strings[1];
        this.Rank = strings[2];
        this.RoomNo = roomno;
        this.TelNo = telno;
    }
    
    static void addInstructor(Instructor instructor) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS `instructors` (`empno` INT NOT NULL, `name` VARCHAR(50) NOT NULL, `rank` VARCHAR(50) NOT NULL, `roomno` INT NOT NULL, `telno` INT NOT NULL, PRIMARY KEY (`empno`), UNIQUE INDEX `empno` (`empno`)) COLLATE='latin1_swedish_ci'");
        
        String query_insert = String.format("INSERT INTO `instructors` (`empno`, `name`, `rank`, `roomno`, `telno`) VALUES (%d, '%s', '%s', %d, %d)", instructor.EmployeeNumber, instructor.Name, instructor.Rank, instructor.RoomNo, instructor.TelNo);
        statement.executeUpdate(query_insert);
    }
    
    static void deleteInstructor(int employeeNumber) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        
        String query_delete = String.format("DELETE FROM `instructors` WHERE  `empno`=%d LIMIT 1", employeeNumber);
        statement.executeUpdate(query_delete);
    }
    
    public static void updateInstructor(Instructor editedInstructor) throws SQLException{
        try{
            deleteInstructor(editedInstructor.EmployeeNumber);
        }catch (SQLException ex){
            
        }finally{
            try{
                addInstructor(editedInstructor);
            }catch (SQLException ex){
                try{
                    String query = String.format("UPDATE `instructors` SET `name`='%s', `rank`='%s', `roomno`=%d, `telno`=%d WHERE `empno`=%d", editedInstructor.Name, editedInstructor.Rank, editedInstructor.RoomNo, editedInstructor.TelNo, editedInstructor.EmployeeNumber);
                    statement.executeUpdate(query);
                }catch (SQLException exx){
                    System.out.println(exx.getMessage());
                    throw new SQLException();
                }
            }
        }
    }
    
    public static void showInstructors() throws SQLException{
        rs = statement.executeQuery("SELECT * FROM `instructors`");
        MySQL.printResultSet(rs, "empno | name | rank | roomno | telno");
    }
}