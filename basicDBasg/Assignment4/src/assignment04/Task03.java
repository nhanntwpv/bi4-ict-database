/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nhan
 */
public class Task03 {

    static Connection connection;
    static Statement statement;
    static ResultSet rs;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            connection = MySQL.initializeConnection();
            statement = connection.createStatement();
            
            statement.executeUpdate("DROP FUNCTION IF EXISTS COMPARE_AVG_GRADE");
            
            statement.executeUpdate("CREATE FUNCTION COMPARE_AVG_GRADE(stno INT(11), cno VARCHAR(5)) RETURNS VARCHAR(50) \n BEGIN \n DECLARE avg_grade, st_grade INT; \n DECLARE comparison VARCHAR(50); \n \n SET avg_grade = (SELECT AVG(`grades`.grade) FROM `grades` WHERE `grades`.`cno` = cno); \n SET st_grade = (SELECT `grades`.grade FROM `grades` WHERE `grades`.`stno` = stno AND `grades`.cno = `cno` LIMIT 1); \n \n IF st_grade < avg_grade THEN \n SET comparison = 'Below average'; \n ELSE \n SET comparison = 'At or above average'; \n END IF; \n \n RETURN comparison; \n END");
            
            rs = statement.executeQuery("SELECT DISTINCT `students`.`name` AS `student`, `courses`.`cname` AS `course`, `instructors`.`name` AS `instructor`, COMPARE_AVG_GRADE(`students`.`stno`, `courses`.`cno`) AS `comparison` FROM `students` JOIN `courses` JOIN `instructors` JOIN `grades` ON `students`.`stno` = `grades`.`stno` AND `courses`.cno = `grades`.`cno` AND `instructors`.`empno` = `grades`.`empno`");
            MySQL.printResultSet(rs, "Student name | Course name | Instructor name | Comparison");
        } catch (SQLException ex) {
            Logger.getLogger(Task03.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}