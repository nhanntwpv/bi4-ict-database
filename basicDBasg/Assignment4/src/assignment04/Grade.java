/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class Grade {
    public int StudentNumber;
    public int EmployeeNumber;
    public String CourseNumber;
    public String Semester;
    public int Year;
    public int Grade;
    
    static Connection connection;
    static Statement statement;
    static ResultSet rs;

    public Grade(int StudentNumber, int EmployeeNumber, String CourseNumber, String Semester, int Year, int Grade) {
        this.StudentNumber = StudentNumber;
        this.EmployeeNumber = EmployeeNumber;
        this.CourseNumber = CourseNumber;
        this.Semester = Semester;
        this.Year = Year;
        this.Grade = Grade;
    }
    
    public Grade(String gradeString){
        String[] strings = gradeString.split(",");
        int stno = Integer.parseInt(strings[0]);
        int empno = Integer.parseInt(strings[1]);
        int year = Integer.parseInt(strings[4]);
        int grade = Integer.parseInt(strings[5]);
        
        this.StudentNumber = stno;
        this.EmployeeNumber = empno;
        this.CourseNumber = strings[2];
        this.Semester = strings[3];
        this.Year = year;
        this.Grade = grade;
    }
    
    static void addGrade(Grade grade) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS `grades` (`stno` INT NOT NULL, `empno` INT NOT NULL, `cno` VARCHAR(5) NOT NULL, `sem` VARCHAR(15) NOT NULL, `year` INT NOT NULL, `grade` INT NOT NULL, CONSTRAINT `FK_student` FOREIGN KEY (`stno`) REFERENCES `students` (`stno`), CONSTRAINT `FK_employee` FOREIGN KEY (`empno`) REFERENCES `instructors` (`empno`), CONSTRAINT `FK_course` FOREIGN KEY (`cno`) REFERENCES `courses` (`cno`)) COLLATE='latin1_swedish_ci'");
        
        String query_insert = String.format("INSERT INTO `grades` (`stno`, `empno`, `cno`, `sem`, `year`, `grade`) VALUES (%d, %d, '%s', '%s', %d, %d)", grade.StudentNumber, grade.EmployeeNumber, grade.CourseNumber, grade.Semester, grade.Year, grade.Grade);
        statement.executeUpdate(query_insert);
    }
    
    static void deleteGrade(int studentNumber, int employeeNumber, String courseNumber) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        
        String query_delete = String.format("DELETE FROM `grades` WHERE  `stno`=%d AND `empno`=%d AND `cno`='%s' LIMIT 1", studentNumber, employeeNumber, courseNumber);
        statement.executeUpdate(query_delete);
    }
    
    public static void updateGrade(Grade editedGrade) throws SQLException{
        try{
            deleteGrade(editedGrade.StudentNumber, editedGrade.EmployeeNumber, editedGrade.CourseNumber);
        }catch (SQLException ex){
            
        }finally{
            try{
                addGrade(editedGrade);
            }catch (SQLException ex){
                try{
                    statement.executeUpdate("TRUNCATE `grades`");
                    updateGrade(editedGrade);
                }catch (SQLException exx){
                    System.out.println(exx.getMessage());
                    throw new SQLException();
                }
            }
        }
    }
    
    public static void showGrades() throws SQLException{
        rs = statement.executeQuery("SELECT * FROM `grades`");
        MySQL.printResultSet(rs, "stno | empno | cno | sem | year | grade");
    }
}