/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class Student {
    public int StudentNumber;
    public String Name;
    public String Address;
    public String City;
    public String State;
    public String Zip;
    
    static Connection connection;
    static Statement statement;
    static ResultSet rs;

    public Student(int StudentNumber, String Name, String Address, String City, String State, String Zip) {
        this.StudentNumber = StudentNumber;
        this.Name = Name;
        this.Address = Address;
        this.City = City;
        this.State = State;
        this.Zip = Zip;
    }
    
    public Student(String studentString){
        String[] strings = studentString.split(",");
        int stno = Integer.parseInt(strings[0]);
        
        this.StudentNumber = stno;
        this.Name = strings[1];
        this.Address = strings[2];
        this.City = strings[3];
        this.State = strings[4];
        this.Zip = strings[5];
    }
    
    static void addStudent(Student student) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS `students` (`stno` INT NOT NULL, `name` VARCHAR(50) NOT NULL, `addr` VARCHAR(250) NOT NULL, `city` VARCHAR(50) NOT NULL, `state` VARCHAR(2) NOT NULL, `zip` VARCHAR(5) NOT NULL, PRIMARY KEY (`stno`), UNIQUE INDEX `stno` (`stno`)) COLLATE='latin1_swedish_ci'");
            
        String query_insert = String.format("INSERT INTO `students` (`stno`, `name`, `addr`, `city`, `state`, `zip`) VALUES (%d, '%s', '%s', '%s', '%s', '%s')", student.StudentNumber, student.Name, student.Address, student.City, student.State, student.Zip);
        statement.executeUpdate(query_insert);
    }
    
    static void deleteStudent(int studentNumber) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
            
        String query_delete = String.format("DELETE FROM `students` WHERE  `stno`=%d LIMIT 1", studentNumber);
        statement.executeUpdate(query_delete);
    }
    
    public static void updateStudent(Student editedStudent) throws SQLException{
        try{
            deleteStudent(editedStudent.StudentNumber);
        }catch (SQLException ex){
            
        }finally{
            try{
                addStudent(editedStudent);
            }catch (SQLException ex){
                try{
                    String query = String.format("UPDATE `students` SET `name`='%s', `addr`='%s', `city`='%s', `state`='%s', `zip`='%s' WHERE  `stno`=%d", editedStudent.Name, editedStudent.Address, editedStudent.City, editedStudent.State, editedStudent.Zip, editedStudent.StudentNumber);
                    statement.executeUpdate(query);
                }catch (SQLException exx){
                    System.out.println(exx.getMessage());
                    throw new SQLException();
                }
            }
        }
    }
    
    public static void showStudents() throws SQLException{
        rs = statement.executeQuery("SELECT * FROM `students`");
        MySQL.printResultSet(rs, "stno | name | addr | city | state | zip");
    }
}