/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class Advising {
    public int StudentNumber;
    public int EmployeeNumber;
    
    static Connection connection;
    static Statement statement;    
    static ResultSet rs;

    public Advising(int StudentNumber, int EmployeeNumber) {
        this.StudentNumber = StudentNumber;
        this.EmployeeNumber = EmployeeNumber;
    }
    
    public Advising(String advisingString){
        String[] strings = advisingString.split(",");
        int stno = Integer.parseInt(strings[0]);
        int empno = Integer.parseInt(strings[1]);
        
        this.StudentNumber = stno;
        this.EmployeeNumber = empno;
    }
    
    static void addAdvising(Advising advising) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS `advising` (`stno` INT NOT NULL, `empno` INT NOT NULL, CONSTRAINT `FK_student_advising` FOREIGN KEY (`stno`) REFERENCES `students` (`stno`), CONSTRAINT `FK_employee_advising` FOREIGN KEY (`empno`) REFERENCES `instructors` (`empno`)) COLLATE='latin1_swedish_ci'");
        
        String query_insert = String.format("INSERT INTO `advising` (`stno`, `empno`) VALUES (%d, %d)", advising.StudentNumber, advising.EmployeeNumber);
        statement.executeUpdate(query_insert);
    }
    
    static void deleteAdvising(int studentNumber, int employeeNumber) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();        
        
        String query_delete = String.format("DELETE FROM `advising` WHERE  `stno`=%d AND `empno`=%d LIMIT 1", studentNumber, employeeNumber);
        statement.executeUpdate(query_delete);
    }
    
    public static void updateAdvising(Advising editedAdvising) throws SQLException{
        try{
            deleteAdvising(editedAdvising.StudentNumber, editedAdvising.EmployeeNumber);
        }catch (SQLException ex){
            
        }finally{
            try{
                addAdvising(editedAdvising);
            }catch (SQLException ex){
                try{
                    statement.executeUpdate("TRUNCATE `advising`");
                    updateAdvising(editedAdvising);
                }catch (SQLException exx){
                    System.out.println(exx.getMessage());
                    throw new SQLException();
                }
            }
        }
    }
    
    public static void showAdvising() throws SQLException{
        rs = statement.executeQuery("SELECT * FROM `advising`");
        MySQL.printResultSet(rs, "stno | empno");
    }
}