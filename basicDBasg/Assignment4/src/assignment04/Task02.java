/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nhan
 */
public class Task02 {
    
    static Connection connection;
    static Statement statement;
    static ResultSet rs;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            // TODO code application logic here
            
            connection = MySQL.initializeConnection();
            statement = connection.createStatement();
            rs = statement.executeQuery("SELECT DISTINCT `instructors`.`name`, `courses`.`cname`, `grades`.`sem`, `grades`.`year`, (SELECT COUNT(`grades`.`stno`) FROM `grades` WHERE `grades`.`cno` = `courses`.`cno` AND `instructors`.`empno` = `grades`.`empno`) AS `number of students`, (SELECT MIN(`grades`.`grade`) FROM `grades` WHERE `grades`.`cno` = `courses`.`cno` AND `instructors`.`empno` = `grades`.`empno`) AS `minimum grade`, (SELECT MAX(`grades`.`grade`) FROM `grades` WHERE `grades`.`cno` = `courses`.`cno` AND `instructors`.`empno` = `grades`.`empno`) AS `maximum grade`, (SELECT AVG(`grades`.`grade`) FROM `grades` WHERE `grades`.`cno` = `courses`.`cno` AND `instructors`.`empno` = `grades`.`empno`) AS `average grade` FROM `instructors` JOIN `courses` JOIN `grades` ON `instructors`.`empno` = `grades`.`empno` AND `grades`.`cno` = `courses`.`cno`");
            
            MySQL.printResultSet(rs, "Instructor name | Course name | Semester | Year | Number of student | Min. grade | Max. grade | Avg. grade");
            
        } catch (SQLException ex) {
            Logger.getLogger(Task02.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}