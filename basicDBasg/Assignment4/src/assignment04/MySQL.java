/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class MySQL {
    public static Connection initializeConnection(){
        Connection conn = null;
        
        // Modify parameters below to suit the environment
        
        String User = "root";
        String Password = "";
        String Server = "localhost";
        String DB = "basicdbasg04";
        int Port = 3306;
        
        try{
            String connString = String.format("jdbc:mysql://%s:%d/%s", Server, Port, DB);
            conn = DriverManager.getConnection(connString, User, Password);
        }catch (SQLException ex){
            System.out.println(ex.getMessage());
            System.exit(69);
        }
        
        return conn;
    }
    
    /**
     * Print out the ResultSet from a SELECT query
     * @param rs the ResultSet will be printed out
     * @param header header of the ResultSet
     * @throws SQLException
     */
    public static void printResultSet(ResultSet rs, String header) throws SQLException{
        ResultSetMetaData rsmd = rs.getMetaData();
        int columnsNumber = rsmd.getColumnCount();
        
        System.out.println("=========================================");
        
        System.out.println(header);

        while (rs.next()) {
            for(int i = 1 ; i <= columnsNumber; i++){
                System.out.print(rs.getString(i) + " | ");
            }
            System.out.println();
        }
        
        System.out.println("=========================================");
    }
}