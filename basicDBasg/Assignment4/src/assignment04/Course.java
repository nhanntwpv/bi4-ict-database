/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment04;

import java.sql.*;

/**
 *
 * @author Nhan
 */
public class Course {
    public String CourseNumber;
    public String CourseName;
    public int Credit;
    public int Capacity;
    
    static Connection connection;
    static Statement statement;    
    static ResultSet rs;

    public Course(String CourseNumber, String CourseName, int Credit, int Capacity) {
        this.CourseNumber = CourseNumber;
        this.CourseName = CourseName;
        this.Credit = Credit;
        this.Capacity = Capacity;
    }
    
    public Course(String courseString){
        String[] strings = courseString.split(",");
        int cr = Integer.parseInt(strings[2]);
        int cap = Integer.parseInt(strings[3]);
        
        this.CourseNumber = strings[0];
        this.CourseName = strings[1];
        this.Credit = cr;
        this.Capacity = cap;
    }
    
    static void addCourse(Course course) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        statement.executeUpdate("CREATE TABLE IF NOT EXISTS `courses` (`cno` VARCHAR(5) NOT NULL, `cname` VARCHAR(250) NOT NULL, `cr` INT NOT NULL, `cap` INT NOT NULL, PRIMARY KEY (`cno`), UNIQUE INDEX `cno` (`cno`)) COLLATE='latin1_swedish_ci'");
        
        String query_insert = String.format("INSERT INTO `courses` (`cno`, `cname`, `cr`, `cap`) VALUES ('%s', '%s', %d, %d)", course.CourseNumber, course.CourseName, course.Credit, course.Capacity);
        statement.executeUpdate(query_insert);
    }
    
    static void deleteCourse(String courseNumber) throws SQLException{
        connection = MySQL.initializeConnection();
        statement = connection.createStatement();
        
        String query_delete = String.format("DELETE FROM `courses` WHERE  `cno`='%s' LIMIT 1", courseNumber);
        statement.executeUpdate(query_delete);
    }
    
    public static void updateCourse(Course editedCourse) throws SQLException{
        try{
            deleteCourse(editedCourse.CourseNumber);
        }catch (SQLException ex){
            
        }finally{
            try{
                addCourse(editedCourse);
            }catch (SQLException ex){
                try{
                    String query = String.format("UPDATE `courses` SET `cname`='%s', `cr`=%d, `cap`=%d WHERE `cno`='%s'", editedCourse.CourseName, editedCourse.Credit, editedCourse.Capacity, editedCourse.CourseNumber);
                    statement.executeUpdate(query);
                }catch (SQLException exx){
                    System.out.println(exx.getMessage());
                    throw new SQLException();
                }
            }
        }
    }
    
    public static void showCourses() throws SQLException{
        rs = statement.executeQuery("SELECT * FROM `courses`");
        MySQL.printResultSet(rs, "cno | cname | cr | cap");
    }
}